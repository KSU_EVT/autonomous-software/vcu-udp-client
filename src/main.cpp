#include <Arduino.h>
#include <NativeEthernet.h>
#include <NativeEthernetUdp.h>
#include <cstdint>

struct vcu_status{
  float speed = 0.0f;
  float steering_angle = 0.0f;
  float brake_torque = 0.0f;
};

struct vcu_control{
  float speed = 0.0f;
  float steer = 0.0f;
  float kp = 0.0f;
};

//NativeEthernet init
byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
const IPAddress vcu_ip(10, 0, 0, 3);
const unsigned int vcu_port = 8888;
const IPAddress pc_ip(10, 0, 0, 2);
const unsigned int pc_port = 7777;

EthernetUDP Udp;

vcu_control control;
vcu_status status;

int count = 0;

char receive_buffer[UDP_TX_PACKET_MAX_SIZE];

void setup() {
  Serial.begin(115200);
  while(!Serial); //wait for serial DISABLE FOR IMPLEMENTATION

  //init ethernet
  Ethernet.begin(mac, vcu_ip);

  if (Ethernet.hardwareStatus() == EthernetNoHardware) {
    Serial.println("Ethernet shield was not found.  Sorry, can't run without hardware. :(");
    while (true) {
      delay(1); // do nothing, no point running without Ethernet hardware
    }
  }
  else{
    Serial.println("Ethernet Hardware Found");
  }

  if (Ethernet.linkStatus() == LinkOFF) {
    Serial.println("Ethernet cable is not connected.");
  }
  else{
    Serial.println(Ethernet.localIP());
  }

  // start UDP
  Udp.begin(vcu_port);

  //test data
  status.speed = 5;
  status.steering_angle = 3;
  status.brake_torque = 0;

  Serial.printf("sizeof(vcu_control): %d\n", sizeof(vcu_control));
  Serial.printf("sizeof(receive_buffer): %d\n", sizeof(receive_buffer));
}

void loop() {
    size_t packet_len = Udp.parsePacket();
    if (packet_len == sizeof(vcu_control)) {
        Udp.read((unsigned char*) &control, packet_len);
        Serial.printf("Got packet size %d! [ ", packet_len);
        for (size_t i = 0; i < packet_len; i++) {
            Serial.printf("%02x ", ((unsigned char*)&control)[i]);
        }
        Serial.printf("]\n");

        Serial.printf("Control; speed: %f, steer: %f, kp: %f\n", control.speed, control.steer, control.kp);
    }

    delay(1500);
    int begin_err = Udp.beginPacket(pc_ip, pc_port);
    size_t len_written = Udp.write((uint8_t*) &status, sizeof(status));
    int end_err = Udp.endPacket();
    //Serial.printf("packet sent, statuses: %d, %d, %d\n", begin_err, len_written, end_err);
    
}

import struct

buf = bytearray(12)
struct.pack_into('<fff', buf, 0, 1.0, 2.0, 3.0)

binfile = open("vcu_control.bin", "wb")
binfile.write(buf)
